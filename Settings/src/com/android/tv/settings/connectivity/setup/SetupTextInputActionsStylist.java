/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.tv.settings.connectivity.setup;

import android.support.v17.leanback.widget.GuidedActionsStylist;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.android.tv.settings.R;

public class SetupTextInputActionsStylist extends GuidedActionsStylist {

        private int id;

        SetupTextInputActionsStylist(int id){
            this.id = id;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v = inflater.inflate(onProvideItemLayoutId(viewType), parent, false);
            return new NextSetupViewHolder(v);
        }

        @Override
        public int onProvideItemLayoutId() {
            return id;
        }

    private static class NextSetupViewHolder extends GuidedActionsStylist.ViewHolder{

        NextSetupViewHolder(View v) {
            super(v);
            View view = v.findViewById(R.id.text_input_next);
            if(view != null){
                view.setOnClickListener(v1 -> getTitleView().onEditorAction(EditorInfo.IME_ACTION_NEXT));
            }
        }
    }

}
